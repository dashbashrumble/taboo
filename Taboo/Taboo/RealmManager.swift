//
//  RealmManager.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static var instance: RealmManager?
    
    var realm: Realm?
    
    fileprivate init() {}
    
    func getRealm() -> Realm {
        return realm!
    }
    
    static func createInstance(){
        instance = RealmManager()
        RealmManager.instance!.createRealm()
    }
    
    fileprivate func closeRealm(){
        realm = nil
    }
    
    fileprivate func createRealm(){
        if realm == nil {
            realm = try! Realm()
        }
    }
    static func closeInstance(){
        RealmManager.instance?.closeRealm()
        self.instance = nil
    }
    
    deinit{
        print("deinit")
    }
    
    func deleteCurrentRealm() {
        let realm = RealmManager.instance?.getRealm()
        if realm != nil {
            realm!.beginWrite()
            realm?.deleteAll()
            try! realm!.commitWrite()
        }
    }
}
