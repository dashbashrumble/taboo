//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"
let y: Float = .
let x: Double = Double(945)/Double(1000)

func printDigits(num: Double, output: [Int]) -> [Int] {
    let timesTen = num*10
    let firstDigit = Int(timesTen)
    var mOutput = output
    mOutput.append(firstDigit)
    let remaining = timesTen - Double(firstDigit)
    if remaining != 0 {
        return printDigits(remaining, output: mOutput)
    } else {
        return mOutput
    }
}

printDigits(Double(x), output: [ ])