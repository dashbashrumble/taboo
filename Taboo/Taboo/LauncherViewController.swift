//
//  ViewController.swift
//  Taboo
//
//  Created by Zilingo on 06/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import GoogleMobileAds

class LauncherViewController: UIViewController {

    @IBOutlet weak var newGame: UIView!
    @IBOutlet weak var stats: UIView!
    @IBOutlet weak var settings: UIView!
    @IBOutlet weak var howTo: UIView!
    @IBOutlet weak var addNewCard: UIView!
    @IBOutlet weak var amIAwesome: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    fileprivate var vibrateTimer: Timer?
    fileprivate var animation: CABasicAnimation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/1876713400"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        UserPreferences.instance.setDefaultValuesForAppVariables()
        RealmManager.createInstance()
        WordListReader.saveAllWords()
        
        addActions()
//        setupVibrateAnimation()
//        scheduleTimers()
        
        let countryCode = (Locale.current as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String
        Answers.logCustomEvent(withName: "User Country",
                                       customAttributes: [
                                        "Country": countryCode])
        
        setHiddenForAllViews(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
        
        setHiddenForAllViews(false)
        
        newGame.slideInFromLeft(0.5)
        addNewCard.slideInFromRight(0.5)
        
        settings.slideInFromLeft(0.7)
        stats.slideInFromRight(0.7)
        
        amIAwesome.slideInFromLeft(0.9)
        howTo.slideInFromRight(0.9)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        setHiddenForAllViews(true)
    }
    
    fileprivate func customizeNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.clear
        self.navigationController!.navigationBar.tintColor = UIColor.clear
    }
    
    fileprivate func setHiddenForAllViews(_ hidden: Bool) {
        addNewCard.isHidden = hidden
        amIAwesome.isHidden = hidden
        newGame.isHidden = hidden
        stats.isHidden = hidden
        settings.isHidden = hidden
        howTo.isHidden = hidden
    }
    
    fileprivate func addActions() {
        let settingsTap = UITapGestureRecognizer(target: self, action: #selector(LauncherViewController.settingsTapped(_:)))
        settings.addGestureRecognizer(settingsTap)
        
        let newGameTap = UITapGestureRecognizer(target: self, action: #selector(LauncherViewController.newGameTapped(_:)))
        newGame.addGestureRecognizer(newGameTap)
        
        let rulesTap = UITapGestureRecognizer(target: self, action: #selector(LauncherViewController.rulesTapped(_:)))
        howTo.addGestureRecognizer(rulesTap)
        
        let newCardTap = UITapGestureRecognizer(target: self, action: #selector(LauncherViewController.addNewCard(_:)))
        addNewCard.addGestureRecognizer(newCardTap)
        
        let statsTap = UITapGestureRecognizer(target: self, action: #selector(LauncherViewController.showStats(_:)))
        stats.addGestureRecognizer(statsTap)
        
        let themesTap = UITapGestureRecognizer(target: self, action: #selector(LauncherViewController.showThemedDecks(_:)))
        amIAwesome.addGestureRecognizer(themesTap)
    }
    
    func newGameTapped(_ sender: UITapGestureRecognizer? = nil) {
        Answers.logCustomEvent(withName: "NewGame", customAttributes: nil)
        self.performSegue(withIdentifier: "NewGame", sender: self)
    }
    
    func settingsTapped(_ sender: UITapGestureRecognizer? = nil) {
        Answers.logCustomEvent(withName: "Settings", customAttributes: nil)
        self.performSegue(withIdentifier: "Settings", sender: self)
    }
    
    func rulesTapped(_ sender: UITapGestureRecognizer? = nil) {
        Answers.logCustomEvent(withName: "ShowRules", customAttributes: nil)
        self.performSegue(withIdentifier: "ShowRules", sender: self)
    }
    
    func addNewCard(_ sender: UITapGestureRecognizer? = nil) {
        Answers.logCustomEvent(withName: "AddNewCard", customAttributes: nil)
        self.performSegue(withIdentifier: "AddNewCard", sender: self)
    }
    
    func showStats(_ sender: UITapGestureRecognizer? = nil) {
        Answers.logCustomEvent(withName: "ShowStats", customAttributes: nil)
        self.performSegue(withIdentifier: "ShowStats", sender: self)
    }
    
    func showThemedDecks(_ sender: UITapGestureRecognizer? = nil) {
        Answers.logCustomEvent(withName: "ShowThemedDecks", customAttributes: nil)
        self.performSegue(withIdentifier: "ShowThemedDecks", sender: self)
    }
    
    fileprivate func scheduleTimers() {
        vibrateTimer = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(LauncherViewController.vibrate), userInfo: nil, repeats: true)
    }
    
    fileprivate func setupVibrateAnimation() {
        animation = CABasicAnimation(keyPath: "position")
        animation!.duration = 0.07
        animation!.repeatCount = 1
        animation!.autoreverses = true
        animation!.fromValue = NSValue(cgPoint: CGPoint(x: newGame.center.x - 5, y: newGame.center.y))
        animation!.toValue = NSValue(cgPoint: CGPoint(x: newGame.center.x + 5, y: newGame.center.y))
    }
    
    func vibrate() {
        newGame.layer.add(animation!, forKey: "position")
    }

}

extension LauncherViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
