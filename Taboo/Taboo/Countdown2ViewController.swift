//
//  Countdown2ViewController.swift
//  Taboo
//
//  Created by Zilingo on 02/11/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class Countdown2ViewController: UIViewController {

    fileprivate var session: Session?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Countdown2ViewController.continueCountdown(_:)), userInfo: nil, repeats: false)
    }
    
    func continueCountdown(_ timer: Timer) {
        let transition = CATransition()
        transition.duration = 1.0
        transition.type = "flip"
        transition.subtype = kCATransitionFromLeft
        
        let countdown1VC =  self.storyboard?.instantiateViewController(withIdentifier: "Countdown1VC") as! Countdown1ViewController
        countdown1VC.updateSession(session!)
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(countdown1VC, animated: false)
    }
    
    func updateSession(_ session: Session) {
        self.session = session
    }

}
