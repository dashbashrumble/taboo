//
//  UserPreferences.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation

class UserPreferences {

    static let instance = UserPreferences()
    
    fileprivate let defaults = UserDefaults.standard
    
    func setWordListVersion(_ version: String) {
        defaults.set(version, forKey: "version")
    }
    
    func getWordListVersion() -> String? {
        if let current = defaults.string(forKey: "version") {
            return current
        }
        return nil
    }
    
    func setDefaultValuesForAppVariables() {
        if getTimeLimit() == nil {
            defaults.set("60", forKey: "timeLimit")
        }
        
        if getWinningPoints() == nil {
            defaults.set("50", forKey: "winningPoints")
        }
        
        if getTaboo() == nil {
            defaults.set("3", forKey: "taboo")
        }
        
        if getPassCount() == nil {
            defaults.set("3", forKey: "passCount")
        }
        
        if getCurrentDeck() == nil {
            setCurrentDeck(WordCategory.MISC.description)
        }
        
        if getRatePromptCount() == 0 {
            setRatePromptCount(1)
        }
    }
    
    func setTimeLimit(_ timeLimit: String) {
        defaults.set(timeLimit, forKey: "timeLimit")
    }
    
    func getTimeLimit() -> String? {
        if let current = defaults.string(forKey: "timeLimit") {
            return current
        }
        return nil
    }
    
    func setTaboo(_ taboo: String) {
        defaults.set(taboo, forKey: "taboo")
    }
    
    func getTaboo() -> String? {
        if let current = defaults.string(forKey: "taboo") {
            return current
        }
        return nil
    }
    
    func setPassCount(_ passCount: String) {
        defaults.set(passCount, forKey: "passCount")
    }
    
    func getPassCount() -> String? {
        if let current = defaults.string(forKey: "passCount") {
            return current
        }
        return nil
    }
    
    func setWinningPoints(_ winningPoints: String) {
        defaults.set(winningPoints, forKey: "winningPoints")
    }
    
    func getWinningPoints() -> String? {
        if let current = defaults.string(forKey: "winningPoints") {
            return current
        }
        return nil
    }
    
    func setHighestScore(_ score: String) {
        defaults.set(score, forKey: "highestScore")
    }
    
    func getHighestScore() -> String? {
        if let current = defaults.string(forKey: "highestScore") {
            return current
        }
        return nil
    }
    
    func setHighestScoreTeam(_ team: String) {
        defaults.set(team, forKey: "highestScoreTeam")
    }
    
    func getHighestScoreTeam() -> String? {
        if let current = defaults.string(forKey: "highestScoreTeam") {
            return current
        }
        return nil
    }
    
    func setHighestScoreDate(_ date: String) {
        defaults.set(date, forKey: "highestScoreDate")
    }
    
    func getHighestScoreDate() -> String? {
        if let current = defaults.string(forKey: "highestScoreDate") {
            return current
        }
        return nil
    }
    
    func setLowestAvg(_ avg: String) {
        defaults.set(avg, forKey: "lowestAvg")
    }
    
    func getLowestAvg() -> String? {
        if let current = defaults.string(forKey: "lowestAvg") {
            return current
        }
        return nil
    }
    
    func setLowestAvgTeam(_ team: String) {
        defaults.set(team, forKey: "lowestAvgTeam")
    }
    
    func getLowestAvgTeam() -> String? {
        if let current = defaults.string(forKey: "lowestAvgTeam") {
            return current
        }
        return nil
    }
    
    func setLowestAvgDate(_ date: String) {
        defaults.set(date, forKey: "lowestAvgDate")
    }
    
    func getLowestAvgDate() -> String? {
        if let current = defaults.string(forKey: "lowestAvgDate") {
            return current
        }
        return nil
    }
    
    func setCurrentDeck(_ deck: String) {
        defaults.set(deck, forKey: "deck")
    }
    
    func getCurrentDeck() -> String? {
        if let current = defaults.string(forKey: "deck") {
            return current
        }
        return nil
    }
    
    func setRatePromptCount(_ ratePromptCount: Int) {
        defaults.set(ratePromptCount, forKey: "ratePromptCount")
    }
    
    func getRatePromptCount() -> Int {
        return defaults.integer(forKey: "ratePromptCount") 
    }
    
    func setRated(_ rated: Bool) {
        defaults.set(rated, forKey: "rated")
    }
    
    func getRated() -> Bool {
        return defaults.bool(forKey: "rated")
    }
    
    func addToRecords(_ record: String) {
        if defaults.array(forKey: "records") != nil {
            var records = defaults.array(forKey: "records") as! [String]
            records.append(record)
            defaults.set(records, forKey: "records")
        } else {
            var records = [String]()
            records.append(record)
            defaults.set(records, forKey: "records")
        }
    }
    
    func getRecords() -> [String] {
        if defaults.array(forKey: "records") != nil {
            return defaults.array(forKey: "records") as! [String]
        }
        
        return [String]()
    }
    
    func setSound(_ sound: Bool) {
        defaults.set(sound, forKey: "sound")
    }
    
    func getSound() -> Bool {
        return defaults.bool(forKey: "sound")
    }
}
