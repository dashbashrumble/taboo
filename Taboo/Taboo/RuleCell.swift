//
//  RuleCell.swift
//  Taboo
//
//  Created by Zilingo on 19/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class RuleCell: UITableViewCell {

    @IBOutlet weak var labelForCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

}
