//
//  DeckViewController.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
//import JLToast
import Toaster
import GoogleMobileAds

class DeckViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createNewDeckButton: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var decks = [String]()
    
    var interstitial: GADInterstitial!
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/4423442546")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/6019595246"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        interstitial = createAndLoadInterstitial()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "DeckCell", bundle: nil), forCellReuseIdentifier: "DeckCell")
        tableView.backgroundColor = UIColor.clear
        
        decks = DeckService.getPersonalDecks()
        
        if decks.count == 0 {
            let zeroResultsView = ZeroResultsView.instanceFromNib() as! ZeroResultsView
            tableView.backgroundView = zeroResultsView
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    @IBAction func createNewDeck(_ sender: AnyObject) {
        showNewDeckNameAlert()
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = "Your Decks"
        self.navigationController?.navigationBar.barTintColor = UIColor.tb_betterBlue()
        self.navigationController?.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    func reloadTableView() {
        decks = DeckService.getPersonalDecks()
        tableView.reloadData()
        
        if decks.count == 0 {
            let zeroResultsView = ZeroResultsView.instanceFromNib() as! ZeroResultsView
            tableView.backgroundView = zeroResultsView
        } else {
            tableView.backgroundView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return decks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeckCell") as! DeckCell
        cell.deckName.text = decks[indexPath.row]
        cell.delegate = self
        cell.index = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ManageYourDeck", sender: decks[indexPath.row])
    }
    
    func showNewDeckNameAlert() {
        let alertController = UIAlertController(title: "New Deck", message: "Please give your deck a unique name", preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let field = alertController.textFields![0] as? UITextField {
                // store your data
                if let deckName = field.text {
                    if deckName.isEmpty {
                        Toast(text: "Your deck wasn't added. Please give a valid deck name", duration: Delay.short).show()
                    } else {
                        if self.checkIfDeckNameIsValid(deckName) {
                            DeckService.addNewDeck(deckName, deckType: DeckType.PERSONAL, ongoingRealmTransaction: false)
                            self.reloadTableView()
                        } else {
                            Toast(text: "Your deck wasn't added. Please give a unique deck name", duration: Delay.short).show()
                        }
                    }
                } else {
                    Toast(text: "Your deck wasn't added. Please give a valid deck name", duration: Delay.short)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Deck Name"
        }
        
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func checkIfDeckNameIsValid(_ deckName: String) -> Bool {
        var isValid = true
        
        for name in decks {
            if name == deckName {
                isValid = false
                break
            }
        }
        
        return isValid
    }
    
    func showDeleteConfirmAlert(_ deck: String) {
        let alertController = UIAlertController(title: "Delete?", message: "Are you sure you want to remove this deck from your collection?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            DeckService.deleteDeck(deck)
            self.reloadTableView()
            Toast(text: "The deck was successfully deleted.", duration: Delay.short).show()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! ManageDeckViewController
        destinationVC.setDeck(sender as! String)
    }
    
}

extension DeckViewController: DeckCellDelegate {
    func deleteClicked(_ deckCell: DeckCell) {
        showDeleteConfirmAlert(decks[deckCell.index])
    }
}

extension DeckViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension DeckViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}
