//
//  WordListJson.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import ObjectMapper

class WordListJson: Mappable {
    fileprivate(set) var version: String?
    fileprivate(set) var wordCards = [WordCardJson]()
    
    required init(map: Map){}
    
    func mapping(map: Map) {
        version <- map["version"]
        wordCards <- map["cards"]
    }
}
