//
//  Countdown3ViewController.swift
//  Taboo
//
//  Created by Zilingo on 02/11/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class Countdown3ViewController: UIViewController {

    @IBOutlet weak var Three: UILabel!
    @IBOutlet weak var Two: UILabel!
    @IBOutlet weak var One: UILabel!
    
    fileprivate var session: Session?
    fileprivate var countdownTimer: Timer?
    var count = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Two.isHidden = true
        One.isHidden = true
        self.navigationItem.setHidesBackButton(true, animated:true)
        scheduleTimers()
    }
    
    func continueCountdown(_ timer: Timer) {
        let transition = CATransition()
        transition.duration = 1.0
        transition.type = "flip"
        transition.subtype = kCATransitionFromLeft
        
        let countdown2VC =  self.storyboard?.instantiateViewController(withIdentifier: "Countdown2VC") as! Countdown2ViewController
        countdown2VC.updateSession(session!)
        self.navigationController?.view.layer.add(transition, forKey: kCATransition)
        self.navigationController?.pushViewController(countdown2VC, animated: false)
    }
    
    func updateSession(_ session: Session) {
        self.session = session
    }
    
    func reset() {
        count = 3
        Three.isHidden = false
        Two.isHidden = true
        One.isHidden = true
        self.navigationItem.setHidesBackButton(true, animated:true)
    }

    func scheduleTimers() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(Countdown3ViewController.tick), userInfo: nil, repeats: true)
    }
    
    func tick() {
        if count == 3 {
            Three.isHidden = true
            Two.isHidden = false
            Two.slideInFromRight(1.0)
            count = count - 1
        } else if count == 2 {
            Two.isHidden = true
            One.isHidden = false
            One.slideInFromRight(1.0)
            count = count - 1
        } else if count == 1 {
            One.isHidden = true
            countdownTimer?.invalidate()
            let transition = CATransition()
            transition.duration = 1.0
            transition.type = "flip"
            transition.subtype = kCATransitionFromLeft
            
            let wordCardVC =  self.storyboard?.instantiateViewController(withIdentifier: "SwipeableWordCardViewController") as! SwipeableWordCardViewController
            wordCardVC.navigationItem.setHidesBackButton(true, animated: true)
            wordCardVC.updateSession(session!)
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.pushViewController(wordCardVC, animated: false)
        }
    }
    
}
