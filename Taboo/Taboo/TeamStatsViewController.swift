//
//  TeamStatsViewController.swift
//  Taboo
//
//  Created by Rahul Manghnani on 1/2/17.
//  Copyright © 2017 Vendetta Works. All rights reserved.
//

import UIKit
import GoogleMobileAds

class TeamStatsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    fileprivate var session: Session?
    fileprivate var teamAPlayers = [String]()
    fileprivate var teamAPlayerScores = [String]()
    fileprivate var teamBPlayers = [String]()
    fileprivate var teamBPlayerScores = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-9043117893678177/8875615454"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        self.navigationItem.title = "Winners - " + session!.getVictoriousTeamName()!
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        tableView.backgroundColor = UIColor.clear
        
        if session != nil {
            session!.updateTeamMVPs()
            
            for (key, value) in session!.teamAPlayerScores {
                teamAPlayers.append(key)
                teamAPlayerScores.append(String(value))
            }
            
            for (key, value) in session!.teamBPlayerScores {
                teamBPlayers.append(key)
                teamBPlayerScores.append(String(value))
            }
        }
    }

    func updateSession(_ session: Session) {
        self.session = session
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return teamAPlayers.count
        } else if section == 1 {
            return teamBPlayers.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        
        if indexPath.section == 0 {
            
            var player = teamAPlayers[indexPath.row]
            if session!.isTeamAMVP(player) {
                player = player + " (MVP)"
            }
            cell.updateCell(player, time: teamAPlayerScores[indexPath.row], isHeader: false)
            
        } else if indexPath.section == 1 {
            
            var player = teamBPlayers[indexPath.row]
            if session!.isTeamBMVP(player) {
                player = player + " (MVP)"
            }
            
            cell.updateCell(player, time: teamBPlayerScores[indexPath.row], isHeader: false)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        
        if section == 0 && teamAPlayers.count > 0 {
            cell.updateCell("Team A Player(s)", time: "Total Points Taken", isHeader: true)
        } else if section == 1 && teamBPlayers.count > 0 {
            cell.updateCell("Team B Player(s)", time: "Total Points Taken", isHeader: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
}

extension TeamStatsViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
