//
//  HistoryCell.swift
//  Taboo
//
//  Created by Zilingo on 03/11/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class HistoryCell: UITableViewCell {

    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var separatorLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }
    
    func updateCell(_ word: String, time: String, isHeader: Bool) {
        wordLabel.text = word
        timeLabel.text = time
        if isHeader {
            separatorLine.isHidden = false
        }
    }

}
