//
//  RecordCell.swift
//  Taboo
//
//  Created by Rahul Manghnani on 1/8/17.
//  Copyright © 2017 Vendetta Works. All rights reserved.
//

import UIKit

class RecordCell: UITableViewCell {

    @IBOutlet weak var recordLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func updateCell(_ record: String) {
        recordLabel.text = record
    }
}
