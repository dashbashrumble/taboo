//
//  RulesViewController.swift
//  Taboo
//
//  Created by Zilingo on 19/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import GoogleMobileAds

class RulesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/3249961679")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-9043117893678177/2483551200"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        interstitial = createAndLoadInterstitial()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "RuleCell", bundle: nil), forCellReuseIdentifier: "RuleCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = "Rules"
        self.navigationController?.navigationBar.barTintColor = UIColor.tb_betterBlue()
        self.navigationController!.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RuleCell") as! RuleCell
        
        switch indexPath.row {
        case 0:
            cell.labelForCell.text = "1. No form or part of ANY word on the card may be given as a clue. Examples: If the word to be guessed is PAYMENT, 'pay' cannot be given as a clue."
            break
        case 1:
            cell.labelForCell.text = "2. If DRINK is a TABOO word, 'drunk' cannot be given as a clue. If SPACESHIP is the word to be guessed, you can't use 'space' or 'ship' as a clue."
            break
        case 2:
            cell.labelForCell.text = "3. No gestures may be made. Examples: You can't form your hand in the shape of a gun as a clue for 'shoot'."
            break
        case 3:
            cell.labelForCell.text = "4. No sound effects or noises may be made, such as explosions and engine noises  "
            break
        case 4:
            cell.labelForCell.text = "5. No initials or abbreviations can be given. Do not say MD if Medical or Doctor is the word to be guessed."
            break
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension RulesViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension RulesViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}

