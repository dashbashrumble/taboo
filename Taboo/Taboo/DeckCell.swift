//
//  DeckCell.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

protocol DeckCellDelegate {
    func deleteClicked(_ deckCell: DeckCell)
}

class DeckCell: UITableViewCell {

    @IBOutlet weak var deckName: UILabel!
    @IBOutlet weak var deleteDeck: UIImageView!
    var index = 0
    
    var delegate: DeckCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action: #selector(DeckCell.imageTapped))
        deleteDeck.isUserInteractionEnabled = true
        deleteDeck.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func imageTapped() {
        delegate?.deleteClicked(self)
    }
}
