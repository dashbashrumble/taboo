//
//  AddNewCardViewController.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
//import JLToast
import Toaster
import GoogleMobileAds

class AddNewCardViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var word: UITextField!
    
    @IBOutlet weak var clue1: UITextField!
    @IBOutlet weak var clue2: UITextField!
    @IBOutlet weak var clue3: UITextField!
    @IBOutlet weak var clue4: UITextField!
    @IBOutlet weak var clue5: UITextField!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBOutlet weak var wordView: UIView!
    @IBOutlet weak var clue1View: UIView!
    @IBOutlet weak var clue2View: UIView!
    @IBOutlet weak var clue3View: UIView!
    @IBOutlet weak var clue4View: UIView!
    @IBOutlet weak var clue5View: UIView!
    
    fileprivate var deck: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        roundedCornersToAllViews()
        
        self.hideKeyboardWhenTappedAround()
        
        word.delegate = self
        clue1.delegate = self
        clue2.delegate = self
        clue3.delegate = self
        clue4.delegate = self
        clue5.delegate = self
        
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddNewCardViewController.removeKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = "New Card"
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.tb_betterBlue()]
        self.navigationController!.navigationBar.tintColor = UIColor.tb_betterBlue()
    }
    
    fileprivate func roundedCornersToAllViews() {
        roundedCorners(wordView)
        roundedCorners(clue1View)
        roundedCorners(clue2View)
        roundedCorners(clue3View)
        roundedCorners(clue4View)
        roundedCorners(clue5View)
    }
    
    fileprivate func roundedCorners(_ view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
            else {
                
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
    @objc func removeKeyboard() {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func setDeck(_ deck: String) {
        self.deck = deck
    }
    
    @IBAction func addNewCard(_ sender: AnyObject) {
//        JLToast.makeText("Your deck wasn't added. Please give a valid deck name", duration: JLToastDelay.ShortDelay).show()
        
        if checkIfWordIsValid() {
            if checkIfCluesAreValid() {
                WordService.addNewWord(word.text!, clue1: clue1.text!, clue2: clue2.text!, clue3: clue3.text!, clue4: clue4.text!, clue5: clue5.text!, difficulty: "Easy", category: "PersonalDeck", deck: deck)
                clearTextFields()
//                JLToast.makeText("Card added successfully! You may continue adding more cards", duration: JLToastDelay.ShortDelay).show()
                Toast(text: "Card added successfully! You may continue adding more cards", duration: Delay.short).show()
            } else {
                Toast(text: "The card wasn't added. You haven't entered all clues!", duration: Delay.short).show()
            }
        } else {
            Toast(text: "The card wasn't added. You haven't entered the unspeakable word!", duration: Delay.short).show()
        }
    }

    fileprivate func clearTextFields() {
        word.text = nil
        clue1.text = nil
        clue2.text = nil
        clue3.text = nil
        clue4.text = nil
        clue5.text = nil
    }
    
    fileprivate func checkIfWordIsValid() -> Bool {
        if word.text == nil || word.text == "" {
            return false
        }
        
        return true
    }
    
    fileprivate func checkIfCluesAreValid() -> Bool {
        if (clue1.text == nil || clue1.text == "") || (clue2.text == nil || clue2.text == "") || (clue3.text == nil || clue3.text == "") || (clue4.text == nil || clue4.text == "") || (clue5.text == nil || clue5.text == "") {
            return false
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
