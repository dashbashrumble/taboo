//
//  VictoryViewController.swift
//  Taboo
//
//  Created by Zilingo on 04/11/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
//import JLToast
import Toaster

class VictoryViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    fileprivate var session: Session?
    @IBOutlet weak var vTeam: UILabel!
    @IBOutlet weak var playerTextField: UITextField!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var addLastPlayerView: UIView!
    @IBOutlet weak var teamStatsButton: UIButton!
    
    fileprivate var vibrateTimer: Timer?
    
    fileprivate var players = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        session!.isLastRound = true
        
        self.hideKeyboardWhenTappedAround()
        
        self.navigationItem.setHidesBackButton(true, animated:true)
        vTeam.text = session?.getVictoriousTeamName()
        
        players = session!.getCurrentTeamPlayers()
        if players.count == 0 {
            pickerView.isHidden = true
        } else {
            playerTextField.text = players[0]
        }
        
        pickerView.dataSource = self
        pickerView.delegate = self
        playerTextField.delegate = self
        
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if session!.mvpSkipped {
            setLastPlayerViewHidden(true)
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VictoryViewController.removeKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if session!.lastPlayerAdded {
            setLastPlayerViewHidden(true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func removeKeyboard() {
        view.endEditing(true)
    }
    
    func updateSession(_ session: Session) {
        self.session = session
    }
    
    func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @IBAction func teamStatsClicked(_ sender: AnyObject) {
        if session!.mvpSkipped {
            Toast(text: "You haven't been adding player names properly for the previous rounds. Sorry, no team stats for you.", duration: Delay.short).show()
        } else {
            if !session!.lastPlayerAdded {
                if playerTextField.text != nil && !playerTextField.text!.isEmpty {
                    if !players.contains(playerTextField.text!) {
                        session!.addNewPlayer(playerTextField.text!)
                    }
                    session!.updatePlayerScores(playerTextField.text, score: session?.turnScore)
                    session!.lastPlayerAdded = true
                    self.performSegue(withIdentifier: "TeamStats", sender: nil)
                } else {
                    Toast(text: "You cannot view team stats without adding the last player.", duration: Delay.short).show()
                }
            } else {
                self.performSegue(withIdentifier: "TeamStats", sender: nil)
            }
        }
    }

    @IBAction func turnStatsClicked(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "TurnStats", sender: nil)
    }
    
    @IBAction func homeClicked(_ sender: AnyObject) {
        showQuitGameAlert()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return players.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return players[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        
        label.font = UIFont(name: "Futura-Medium", size: 13)!
        label.text = players[row]
        label.textColor = UIColor.tb_yellow()
        label.textAlignment = .center
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        playerTextField.text = players[row]
    }
    
    func setLastPlayerViewHidden(_ hidden: Bool) {
        self.addLastPlayerView.isHidden = hidden
    }
    
    func showQuitGameAlert() {
        let alertController = UIAlertController(title: "Quit game?", message: "Are you sure you want to quit the game? You will lose all your progress!", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (_) in }
        
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "TurnStats":
            let destinationVC = segue.destination as! ResultViewController
            destinationVC.setSession(session!)
        case "TeamStats":
            let destinationVC = segue.destination as! TeamStatsViewController
            destinationVC.updateSession(session!)
        default:
            break
        }
    }
}
