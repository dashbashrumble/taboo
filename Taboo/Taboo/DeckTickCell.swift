//
//  DeckTickCell.swift
//  Taboo
//
//  Created by Zilingo on 12/11/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class DeckTickCell: UITableViewCell {

    @IBOutlet weak var deckTitleLabel: UILabel!
    @IBOutlet weak var tickImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        unTickCell()
    }

    func updateCell(_ title: String) {
        deckTitleLabel.text = title
    }
    
    func tickCell() {
        tickImageView.image = UIImage(named: Images.TICK)
    }
    
    func unTickCell() {
        tickImageView.image = UIImage(named: Images.UNTICK)
    }
}
