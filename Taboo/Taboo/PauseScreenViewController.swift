//
//  PauseScreenViewController.swift
//  Taboo
//
//  Created by Zilingo on 18/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class PauseScreenViewController: UIViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    fileprivate var pauseScreenDelegate: PauseScreenDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.clear
        view.isOpaque = false
    }
    
    func setDelegate(_ pauseScreenDelegate: PauseScreenDelegate) {
        self.pauseScreenDelegate = pauseScreenDelegate
    }
    
    @IBAction func homePressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
        pauseScreenDelegate?.returnToHomeScreen()
    }

    @IBAction func continuePressed(_ sender: AnyObject) {
        pauseScreenDelegate?.resume()
        self.dismiss(animated: true, completion: {})
    }

}
