//
//  HistoryViewController.swift
//  Taboo
//
//  Created by Rahul Manghnani on 12/28/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    fileprivate var session: Session?
    fileprivate var words = [String]()
    fileprivate var times = [String]()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/6179377347")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/5918279621"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        interstitial = createAndLoadInterstitial()
        
        if let wordHistory = session?.wordHistory {
            for historyWord in wordHistory {
                words.append(historyWord.word!)
                times.append(String(historyWord.timeTaken!))
            }
        }
        
        self.navigationItem.title = "Words Seen (" + String(words.count) + ")"
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        tableView.backgroundColor = UIColor.clear
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.navigationController!.navigationBar.frame.size.width, height: self.navigationController!.navigationBar.frame.size.height))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        let color = UIColor.tb_betterBlue()
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cgImage = image?.cgImage
        let finalImage = UIImage(cgImage: cgImage!)
        
        self.navigationController?.navigationBar.setBackgroundImage(finalImage, for: .default)
        
        self.navigationController?.navigationBar.barTintColor = UIColor.tb_betterBlue()
        self.navigationController?.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    func updateSession(_ session: Session) {
        self.session = session
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return words.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        cell.updateCell(String(indexPath.row + 1) + ". " + words[indexPath.row], time: times[indexPath.row], isHeader: false)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        cell.updateCell("Word", time: "Time Taken(s)", isHeader: true)
        return cell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
}

extension HistoryViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension HistoryViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}
