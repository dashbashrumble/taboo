//
//  StatCell.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class StatCell: UITableViewCell {

    @IBOutlet weak var stat: UILabel!
    @IBOutlet weak var team: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var statValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func updateCell(_ stat: String?, team: String?, date: String?, statValue: String?) {
        self.stat.text = stat
        self.team.text = team
        self.date.text = date
        self.statValue.text = statValue
    }
}
