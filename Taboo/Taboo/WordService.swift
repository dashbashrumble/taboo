//
//  WordService.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import RealmSwift

class WordService {
    static func getAllWords() -> [WordCard] {
        let realm = RealmManager.instance?.getRealm()
        
        let words = realm?.objects(WordCard)
        var wordCards = [WordCard]()
        for word in words! {
            wordCards.append(word)
        }
        
        return wordCards
    }
    
    static func addNewWord(_ word: String, clue1: String, clue2: String, clue3: String, clue4: String, clue5: String, difficulty: String, category: String, deck: String?) {
        let realm = RealmManager.instance?.getRealm()
        
        realm?.beginWrite()

        let wordCard = WordCard()
        wordCard.word = word
        
        let clues = List<Clue>()
        
        let clueOne = Clue()
        clueOne.clue = clue1
        clues.append(clueOne)
        
        let clueTwo = Clue()
        clueTwo.clue = clue2
        clues.append(clueTwo)
        
        let clueThree = Clue()
        clueThree.clue = clue3
        clues.append(clueThree)
        
        let clueFour = Clue()
        clueFour.clue = clue4
        clues.append(clueFour)
        
        let clueFive = Clue()
        clueFive.clue = clue5
        clues.append(clueFive)
        
        wordCard.clues = clues
        
        wordCard.difficulty = difficulty
        wordCard.category = category
        
        if deck != nil {
            let wordDeck = DeckService.getDeck(deck!)
            wordCard.deck = wordDeck
        }
        
        realm?.add(wordCard, update: false)
        
        try! realm?.commitWrite()
        
        DeckService.addCardToDeck(deck!, word: wordCard)
    }
    
    static func deleteWord(_ wordCard: WordCard) {
        let realm = RealmManager.instance?.getRealm()
        realm?.beginWrite()
        realm?.delete(wordCard)
        try! realm?.commitWrite()
    }
    
}
