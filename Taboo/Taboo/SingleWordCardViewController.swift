//
//  SingleWordCardViewController.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SingleWordCardViewController: UIViewController {

    @IBOutlet weak var word: UILabel!
    
    @IBOutlet weak var clue1: UILabel!
    @IBOutlet weak var clue2: UILabel!
    @IBOutlet weak var clue3: UILabel!
    @IBOutlet weak var clue4: UILabel!
    @IBOutlet weak var clue5: UILabel!
    @IBOutlet weak var bannerView: GADBannerView!
    
    fileprivate var wordCard: WordCard?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-9043117893678177/5880704886"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        word.text = wordCard?.word
        
        var clues = [String]()
        for clue in (wordCard?.clues)! {
            clues.append(clue.clue)
        }
        clue1.text = clues[0]
        clue2.text = clues[1]
        clue3.text = clues[2]
        clue4.text = clues[3]
        clue5.text = clues[4]
        
        roundedCornersToAllViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = ""
        self.navigationController!.navigationBar.barTintColor = UIColor.yellow
        self.navigationController!.navigationBar.tintColor = UIColor.tb_betterBlue()
    }
    
    fileprivate func roundedCornersToAllViews() {
        roundedCorners(word)
        roundedCorners(clue1)
        roundedCorners(clue2)
        roundedCorners(clue3)
        roundedCorners(clue4)
        roundedCorners(clue5)
    }
    
    fileprivate func roundedCorners(_ view: UIView) {
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
    }
    
    func setWordCard(_ wordCard: WordCard) {
        self.wordCard = wordCard
    }
}

extension SingleWordCardViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
