//
//  ThemedDecksViewController.swift
//  Taboo
//
//  Created by Zilingo on 12/11/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
//import JLToast
import Toaster
import GoogleMobileAds

class ThemedDecksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/7325648496")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    fileprivate var systemDecks = [Deck]()
    fileprivate var personalDecks = [Deck]()
    fileprivate var currentDeck = UserPreferences.instance.getCurrentDeck()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-9043117893678177/6619071488"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        interstitial = createAndLoadInterstitial()
        
        systemDecks = DeckService.getDeckListForDeckType(DeckType.SYSTEM)
        personalDecks = DeckService.getDeckListForDeckType(DeckType.PERSONAL)
        
        tableView.register(UINib(nibName: "DeckTickCell", bundle: nil), forCellReuseIdentifier: "DeckTickCell")
        tableView.register(UINib(nibName: "EmptySectionWarningCell", bundle: nil), forCellReuseIdentifier: "EmptySectionWarningCell")
        tableView.register(UINib(nibName: "DeckHeaderCell", bundle: nil), forCellReuseIdentifier: "DeckHeaderCell")
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    fileprivate func customizeNavigationBar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.tb_betterBlue()
        self.navigationController?.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            
            if systemDecks.count == 0 {
                return 1
            } else {
                return systemDecks.count
            }
            
        } else if section == 1 {
            
            if personalDecks.count == 0 {
                return 1
            } else {
                return personalDecks.count
            }
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        if indexPath.section == 0 {
            
            if systemDecks.count == 0 {
                let warningCell = tableView.dequeueReusableCell(withIdentifier: "EmptySectionWarningCell") as! EmptySectionWarningCell
                warningCell.updateCell("You have no pre-loaded decks.")
                cell = warningCell
            } else {
                let deckCell = tableView.dequeueReusableCell(withIdentifier: "DeckTickCell") as! DeckTickCell
                deckCell.updateCell(systemDecks[indexPath.row].word)
                
                if currentDeck == systemDecks[indexPath.row].word {
                    deckCell.tickCell()
                } else {
                    deckCell.unTickCell()
                }
                
                cell = deckCell
            }
            
        } else if indexPath.section == 1 {
            
            if personalDecks.count == 0 {
                let warningCell = tableView.dequeueReusableCell(withIdentifier: "EmptySectionWarningCell") as! EmptySectionWarningCell
                warningCell.updateCell("You have no personalized decks yet.")
                cell = warningCell
            } else {
                let deckCell = tableView.dequeueReusableCell(withIdentifier: "DeckTickCell") as! DeckTickCell
                deckCell.updateCell(personalDecks[indexPath.row].word)
                
                if currentDeck == personalDecks[indexPath.row].word {
                    deckCell.tickCell()
                } else {
                    deckCell.unTickCell()
                }
                
                cell = deckCell
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var selectedDeck = ""
        
        if indexPath.section == 0 {
            selectedDeck = systemDecks[indexPath.row].word
        } else if indexPath.section == 1 {
            selectedDeck = personalDecks[indexPath.row].word
        }
        
        if DeckService.isDeckEmpty(selectedDeck) {
            Toast(text: "This deck cannot be used since it has no words. Please add new words to the deck before selecting it.", duration: Delay.short).show()
        } else {
            currentDeck = selectedDeck
            tableView.reloadData()
            UserPreferences.instance.setCurrentDeck(currentDeck!)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "DeckHeaderCell") as! DeckHeaderCell
        if section == 0 {
            headerCell.updateCell("System Decks")
        } else {
            headerCell.updateCell("Your Decks")
        }
        return headerCell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}

extension ThemedDecksViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension ThemedDecksViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}
