//
//  CardView.swift
//  Taboo
//
//  Created by Zilingo on 21/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import UIKit

class CardView: UIView {
    
    @IBOutlet weak var wordLabel: UILabel!
    @IBOutlet weak var clueLabel: UILabel!
    
    var overlayView: OverlayView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        // Background
        self.backgroundColor = UIColor.white
        
        // Shadow
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.25
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        layer.shadowRadius = 4.0
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
        
        // Corner Radius
        layer.cornerRadius = 10.0;
    }
    
    func setupWordCard(_ word: String, clue1: String, clue2: String, clue3: String, clue4: String, clue5: String) {
        wordLabel.text = word
        clueLabel.text = clue1 + "\n\n" + clue2 + "\n\n" + clue3 + "\n\n" + clue4 + "\n\n" + clue5
    }
    
    func addOverlay() {
        overlayView = OverlayView(frame: self.bounds)
        overlayView!.alpha = 0;
        overlayView?.layer.cornerRadius = 10.0
        overlayView?.layer.masksToBounds = true
        self.addSubview(overlayView!)
    }
    
    class OverlayView: UIView {

        override init(frame: CGRect) {
            super.init(frame: frame)
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
        }
        
        func setMode(_ mode: OverlayMode) {
            if mode == OverlayMode.left {
                self.backgroundColor = UIColor.red
                self.alpha = 1
            } else if mode == OverlayMode.right {
                self.backgroundColor = UIColor.green
                self.alpha = 1
            }
        }
        
        func setClearColor() {
            self.backgroundColor = UIColor.clear
            self.alpha = 0
        }
    }
    
}

enum OverlayMode {
    case left
    case right
}
