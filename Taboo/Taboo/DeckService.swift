//
//  DeckService.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation

class DeckService {
    static func getDecks() -> [String] {
        let realm = RealmManager.instance?.getRealm()
        
        let decks = realm?.objects(Deck)
        var wordDecks = [String]()
        for deck in decks! {
            wordDecks.append(deck.word)
        }
        
        return wordDecks
    }
    
    static func getPersonalDecks() -> [String] {
        let realm = RealmManager.instance?.getRealm()
        
        let decks = realm?.objects(Deck).filter("type = '" + DeckType.PERSONAL.rawValue + "'")
        var wordDecks = [String]()
        for deck in decks! {
            wordDecks.append(deck.word)
        }
        
        return wordDecks
    }
    
    static func addNewDeck(_ deckName: String, deckType: DeckType, ongoingRealmTransaction: Bool) -> Deck {
        let realm = RealmManager.instance?.getRealm()
        
        if !ongoingRealmTransaction {
            realm?.beginWrite()
        }

        let deck = Deck()
        deck.word = deckName
        deck.type = deckType.rawValue
        
        realm?.add(deck, update: false)
        
        if !ongoingRealmTransaction {
            try! realm?.commitWrite()
        }
        
        return deck
    }
    
    static func getDeck(_ deckName: String) -> Deck? {
        let realm = RealmManager.instance?.getRealm()
        return realm?.object(ofType: Deck.self, forPrimaryKey: deckName as AnyObject)
    }
    
    static func getOrCreateDeck(_ deckName: String, deckType: DeckType, ongoingRealmTransaction: Bool) -> Deck {
        let realm = RealmManager.instance?.getRealm()
        var deck = realm?.object(ofType: Deck.self, forPrimaryKey: deckName as AnyObject)
        
        if deck == nil {
            deck = addNewDeck(deckName, deckType: deckType, ongoingRealmTransaction: ongoingRealmTransaction)
        }
        
        return deck!
    }
    
    static func addCardToDeck(_ deckName: String, word: WordCard) {
        let realm = RealmManager.instance?.getRealm()
        
        let deck = realm?.object(ofType: Deck.self, forPrimaryKey: deckName as AnyObject)
        
        realm?.beginWrite()
        deck?.cards.append(word)
        realm?.add(deck!, update: true)
        try! realm?.commitWrite()
    }
    
    static func getWordsForDeck(_ deckName: String) -> [WordCard] {
        var cards = [WordCard]()
        
        let realm = RealmManager.instance?.getRealm()
        let deck =  realm?.object(ofType: Deck.self, forPrimaryKey: deckName as AnyObject)
        
        if let wordDeck = deck {
            let wordList = wordDeck.cards
            for word in wordList {
                cards.append(word)
            }
        }
        
        return cards
    }
    
    static func isDeckEmpty(_ deckName: String) -> Bool {
        let realm = RealmManager.instance?.getRealm()
        let deck =  realm?.object(ofType: Deck.self, forPrimaryKey: deckName as AnyObject)
        if let wordDeck = deck {
            if wordDeck.cards.count > 0 {
                return false
            }
        }
        
        return true
    }
    
    static func getDeckListForDeckType(_ deckType: DeckType) -> [Deck] {
        let realm = RealmManager.instance?.getRealm()
        let rDecks = realm?.objects(Deck).filter("type = '" + deckType.rawValue + "'")
        var decks = [Deck]()
        if rDecks != nil {
            for rDeck in rDecks! {
                decks.append(rDeck)
            }
        }
        return decks
    }
    
    static func deleteDeck(_ deckName: String) {
        let realm = RealmManager.instance?.getRealm()
        let deck =  realm?.object(ofType: Deck.self, forPrimaryKey: deckName as AnyObject)
        if let wordDeck = deck {
            realm?.beginWrite()
            realm?.delete(wordDeck)
            try! realm?.commitWrite()
        }
    }
}
