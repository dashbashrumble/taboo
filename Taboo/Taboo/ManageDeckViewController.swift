//
//  ManageDeckViewController.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ManageDeckViewController: UIViewController {

    @IBOutlet weak var newCardButton: UIButton!
    @IBOutlet weak var deleteCardsButton: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!
    
    fileprivate var deck: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/2291979516"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = deck!
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.tb_yellow()]
        self.navigationController!.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    func setDeck(_ deck: String) {
        self.deck = deck
    }

    @IBAction func addNewCard(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "AddNewWordCard", sender: nil)
    }
    
    @IBAction func deleteCards(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "ViewCards", sender: nil)
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddNewWordCard" {
            let destinationVC = segue.destination as! AddNewCardViewController
            destinationVC.setDeck(deck!)
        } else if segue.identifier == "ViewCards" {
            let destinationVC = segue.destination as! ViewCardsViewController
            destinationVC.setDeckName(deck!)
        }
    }
}

extension ManageDeckViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

