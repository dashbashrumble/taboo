//
//  ZeroResultsView.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class ZeroResultsView: UIView {

    @IBOutlet weak var labelForView: UILabel!
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "ZeroResultsView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    func setLabelText(_ message: String) {
        self.labelForView.text = message
    }
}
