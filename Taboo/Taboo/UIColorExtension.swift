//
//  UIColorExtension.swift
//  Taboo
//
//  Created by Zilingo on 09/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation

import UIKit

extension UIColor{
    
    //MARK: - Taboo Colors
    
    static func tb_yellow() -> UIColor {
        return ColorService(hexValue: "#F9C8OE").getUIColor()
    }
    
    static func tb_pink() -> UIColor {
        return ColorService(hexValue: "#DB504A").getUIColor()
    }
    
    static func tb_purple() -> UIColor {
        return ColorService(hexValue: "#586BA4").getUIColor()
    }
    
    static func tb_blue() -> UIColor {
        return ColorService(hexValue: "#028090").getUIColor()
    }
    
    static func tb_betterBlue() -> UIColor {
        return ColorService(hexValue: "#0191C8").getUIColor()
    }
}