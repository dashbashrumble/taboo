//
//  EmptySectionWarningCell.swift
//  Taboo
//
//  Created by Zilingo on 30/11/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class EmptySectionWarningCell: UITableViewCell {

    @IBOutlet weak var warningLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.isUserInteractionEnabled = false
    }
    
    func updateCell(_ message: String) {
        warningLabel.text = message
    }
}
