//
//  ViewCardsViewController.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
//import JLToast
import Toaster
import GoogleMobileAds

class ViewCardsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    fileprivate var deckName: String?
    fileprivate var cards = [WordCard]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-9043117893678177/2160241286"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "DeckCell", bundle: nil), forCellReuseIdentifier: "DeckCell")
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        
        updateDatasource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = "Your Cards"
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.navigationController!.navigationBar.frame.size.width, height: self.navigationController!.navigationBar.frame.size.height))
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        let color = UIColor.tb_betterBlue()
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let cgImage = image?.cgImage
        let finalImage = UIImage(cgImage: cgImage!)
        
        self.navigationController?.navigationBar.setBackgroundImage(finalImage, for: .default)

        self.navigationController!.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    fileprivate func updateDatasource() {
        cards = DeckService.getWordsForDeck(deckName!)
        
        if cards.count == 0 {
            let zeroResultsView = ZeroResultsView.instanceFromNib() as! ZeroResultsView
            zeroResultsView.setLabelText("This deck has no cards.")
            tableView.backgroundView = zeroResultsView
        }
    }
    
    func setDeckName(_ deckName: String) {
        self.deckName = deckName
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeckCell") as! DeckCell
        cell.deckName.text = cards[indexPath.row].word
        cell.delegate = self
        cell.index = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let wordCard = cards[indexPath.row]
        self.performSegue(withIdentifier: "ShowSingleCard", sender: wordCard)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! SingleWordCardViewController
        destinationVC.setWordCard(sender as! WordCard)
    }
    
    func showDeleteConfirmAlert(_ wordCard: WordCard) {
        let alertController = UIAlertController(title: "Delete?", message: "Are you sure you want to remove this word from your deck?", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            WordService.deleteWord(wordCard)
            self.updateDatasource()
            self.tableView.reloadData()
            Toast(text: "The card was successfully deleted.", duration: Delay.short).show()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension ViewCardsViewController: DeckCellDelegate {
    func deleteClicked(_ deckCell: DeckCell) {
        showDeleteConfirmAlert(cards[deckCell.index])
    }
}

extension ViewCardsViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

