//
//  TeamNamesViewController.swift
//  Taboo
//
//  Created by Zilingo on 13/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import GoogleMobileAds

class TeamNamesViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var teamA: UITextField!
    @IBOutlet weak var teamB: UITextField!
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var rateUsView: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var session = Session()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-9043117893678177/6869642488"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0

        self.hideKeyboardWhenTappedAround()
        customizeTextFields()
        
        teamA.delegate = self
        teamB.delegate = self
        
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        rateUsView.isHidden = true
        
        if UserPreferences.instance.getRatePromptCount() % 5 == 0 && !UserPreferences.instance.getRated() {
            rateUsView.isHidden = false
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(TeamNamesViewController.removeKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func removeKeyboard() {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customizeNavigationBar()
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = "Enter Team Names"
        self.navigationController?.navigationBar.barTintColor = UIColor.tb_betterBlue()
        self.navigationController!.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
            else {
                
            }
        }
        
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
            else {
                
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    fileprivate func customizeTextFields() {
        teamA.placeholder = "Team A"
        teamA.text = "Team A"
        teamB.placeholder = "Team B"
        teamB.text = "Team B"
    }
    
    fileprivate func customizeStartGameButton() {
        startGameButton.layer.masksToBounds = false
        
        startGameButton.layer.shadowColor = UIColor.black.cgColor;
        startGameButton.layer.shadowOpacity = 0.8
        startGameButton.layer.shadowRadius = 12
        startGameButton.layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
    }

    @IBAction func startNewGame(_ sender: AnyObject) {
        if teamA.text != nil && teamA.text != "" && teamB.text != nil && teamB.text != "" {
            session.setTeamA(teamA.text!)
            session.setTeamB(teamB.text!)
            
            let transition = CATransition()
            transition.duration = 1.0
            transition.type = "flip"
            transition.subtype = kCATransitionFromLeft
            
            let countdown3VC =  self.storyboard?.instantiateViewController(withIdentifier: "Countdown3VC") as! Countdown3ViewController
            countdown3VC.updateSession(session)
            self.navigationController?.view.layer.add(transition, forKey: kCATransition)
            self.navigationController?.pushViewController(countdown3VC, animated: false)
            
        } else {
            showInvalidTeamNamesAlert()
        }
    }
    
    @IBAction func rateUsClicked(_ sender: AnyObject) {
        Answers.logCustomEvent(withName: "Rate Us Clicked",
                                       customAttributes: [
                                        "Rate Us": "Clicked"])
        
        UserPreferences.instance.setRated(true)
        let url = URLComponents(string: "itms-apps://itunes.apple.com/app/id1199874005")!
        UIApplication.shared.openURL(url.url!)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as? SwipeableWordCardViewController
        destinationVC?.updateSession(session)
    }
 
    
    fileprivate func showInvalidTeamNamesAlert() {
        let alert = UIAlertController(title: "Invalid Team Names!", message: "Please enter valid team names.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension TeamNamesViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}
