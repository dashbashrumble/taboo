//
//  Session.swift
//  Taboo
//
//  Created by Zilingo on 13/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation

enum Turn {
    case teama
    case teamb
}

class Session {
    var teamA = "Team A"
    var teamB = "Team B"
    fileprivate var currentTurn: Turn?
    
    fileprivate var teamAScores = [Int]()
    fileprivate var teamBScores = [Int]()
    
    fileprivate var teamACorrect = [Int]()
    fileprivate var teamBCorrect = [Int]()
    
    fileprivate var teamATaboo = [Int]()
    fileprivate var teamBTaboo = [Int]()
    
    fileprivate(set) var turnScore = 0
    fileprivate(set) var turnTaboo = 0
    fileprivate(set) var turnCorrect = 0
    
    fileprivate(set) var teamATotalScore = 0
    fileprivate(set) var teamBTotalScore = 0
    
    fileprivate(set) var aspw = Double(0)
    
    fileprivate(set) var wordCaptures = WordCaptures()
    
    fileprivate(set) var streak = 0
    
    fileprivate(set) var victoriousTeam: Turn?
    fileprivate(set) var victoriousTeamName: String?
    
    var record = ""
    
    var mvpSkipped = false
    var lastPlayerAdded = false
    var isLastRound = false
    
    // For history
    var wordHistory = [HistoryWord]()
    
    // For MVP calculation
    fileprivate(set) var teamAPlayers = [String]()
    fileprivate(set) var teamBPlayers = [String]()
    fileprivate(set) var teamAPlayerScores = [String : Int]()
    fileprivate(set) var teamBPlayerScores = [String : Int]()
    fileprivate(set) var teamAMVPs = [String]()
    fileprivate(set) var teamBMVPs = [String]()
    
    func setTeamA(_ teamA: String) {
        self.teamA = teamA
    }
    
    func setTeamB(_ teamB: String) {
        self.teamB = teamB
    }
    
    func getCurrentTurn() -> Turn {
        if let turn = currentTurn {
            if turn == Turn.teama {
                currentTurn = Turn.teamb
            } else {
                currentTurn = Turn.teama
            }
        } else {
            currentTurn = Turn.teama
        }
        
        return currentTurn!
    }
    
    func getCurrentTeamName(_ turn: Turn) -> String {
        if turn == Turn.teama {
            return teamA
        } else {
            return teamB
        }
    }
    
    func getCurrentTeamPlayers() -> [String] {
        if currentTurn! == Turn.teama {
            return teamAPlayers
        } else {
            return teamBPlayers
        }
    }
    
    func reportCorrect(_ correct: Int) {
        self.turnCorrect = correct
        
        if currentTurn == Turn.teama {
            teamACorrect.append(correct)
        } else {
            teamBCorrect.append(correct)
        }
    }
    
    func reportTaboo(_ taboo: Int) {
        self.turnTaboo = taboo
        
        if currentTurn == Turn.teama {
            teamATaboo.append(taboo)
        } else {
            teamBTaboo.append(taboo)
        }

    }
    
    func reportScore(_ score: Int) {
        self.turnScore = score
        
        if currentTurn == Turn.teama {
            teamAScores.append(score)
            teamATotalScore = teamATotalScore + score
        } else {
            teamBScores.append(score)
            teamBTotalScore = teamBTotalScore + score
        }
    }
    
    func addToHistory(_ word: String, timeTaken: Int) {
        wordHistory.append(HistoryWord(word: word, timeTaken: timeTaken))
    }
    
    func calculateAverageSecondsPerWord(_ times: [Int]) {
        var sum = Double(0)
        for time in times {
            sum = sum + Double(time)
        }
        
        aspw = sum / Double(times.count)
        aspw = Double(round(1000*aspw)/1000)
    }
    
    func reportWordCaptures(_ wordCaptures: WordCaptures) {
        self.wordCaptures = wordCaptures
    }
    
    func reportStreak(_ streak: Int) {
        self.streak = streak
    }
    
    func getBoogieString() -> String {
        return wordCaptures.boogieWord + " (" + String(wordCaptures.boogieTime) + "s)"
    }
    
    func getGloryString() -> String {
        return wordCaptures.gloryWord + " (" + String(wordCaptures.gloryTime) + "s)"
    }
    
    func resetTurnVariables() {
        turnScore = 0
        turnTaboo = 0
        turnCorrect = 0
        aspw = Double(0)
        wordCaptures = WordCaptures()
        streak = 0
        wordHistory = [HistoryWord]()
    }
    
    func reportVictory() -> Turn? {
        let wp = Int(UserPreferences.instance.getWinningPoints()!)!
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let today = formatter.string(from: date)
        
        if teamATotalScore >= wp {
            victoriousTeam = Turn.teama
            victoriousTeamName = teamA
            record = teamA + " beat " + teamB + " on " + today
            UserPreferences.instance.addToRecords(record)
            return Turn.teama
        } else  if teamBTotalScore >= wp {
            victoriousTeam = Turn.teamb
            victoriousTeamName = teamB
            record = teamB + " beat " + teamA + " on " + today
            UserPreferences.instance.addToRecords(record)
            return Turn.teamb
        }
        
        return nil
    }
    
    func getVictoriousTeamName() -> String? {
        return victoriousTeamName
    }
    
    func addNewPlayer(_ player: String) {
        if currentTurn! == Turn.teama {
            teamAPlayers.append(player)
        } else {
            teamBPlayers.append(player)
        }
    }
    
    func updatePlayerScores(_ player: String?, score: Int?) {
        if player != nil && score != nil {
            if currentTurn! == Turn.teama {
                if teamAPlayerScores[player!] != nil {
                    let previousScore = teamAPlayerScores[player!]!
                    teamAPlayerScores[player!] = previousScore + score!
                } else {
                    teamAPlayerScores[player!] = score!
                }
            } else {
                if teamBPlayerScores[player!] != nil {
                    let previousScore = teamBPlayerScores[player!]!
                    teamBPlayerScores[player!] = previousScore + score!
                } else {
                    teamBPlayerScores[player!] = score!
                }
            }
        }
    }
    
    func updateTeamMVPs() {
        var teamAMax = -1000
        var teamBMax = -1000
        
        for (_, value) in teamAPlayerScores {
            if value > teamAMax {
                teamAMax = value
            }
        }
        
        for (_, value) in teamBPlayerScores {
            if value > teamBMax {
                teamBMax = value
            }
        }
        
        for (key, value) in teamAPlayerScores {
            if value == teamAMax {
                teamAMVPs.append(key)
            }
        }
        
        for (key, value) in teamBPlayerScores {
            if value == teamBMax {
                teamBMVPs.append(key)
            }
        }
    }
    
    func isTeamAMVP(_ player: String) -> Bool {
        if teamAMVPs.contains(player) {
            return true
        }
        
        return false
    }
    
    func isTeamBMVP(_ player: String) -> Bool {
        if teamBMVPs.contains(player) {
            return true
        }
        
        return false
    }
}

class HistoryWord {
    var word: String?
    var timeTaken: Int?
    
    init(word: String?, timeTaken: Int?) {
        self.word = word
        self.timeTaken = timeTaken
    }
}
