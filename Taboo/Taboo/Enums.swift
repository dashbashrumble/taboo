//
//  Enums.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation

enum WordDifficulty: String {
    case EASY = "EASY"
    case MEDIUM = "MEDIUM"
    case HARD = "HARD"
}

enum WordCategory: String {
    case MISC = "MISC"
    case INTERCELEB = "InterCeleb"
    
    var description: String {
        switch self {
        case .MISC:
            return "Miscellaneous"
        case .INTERCELEB:
            return "International Celebrities"
        }
    }
}

enum DeckType: String {
    case SYSTEM = "SYSTEM"
    case PERSONAL = "PERSONAL"
}