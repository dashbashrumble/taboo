//
//  Deck.swift
//  Taboo
//
//  Created by Zilingo on 23/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import RealmSwift

class Deck: Object {
    dynamic var word = ""
    var cards = List<WordCard>()
    dynamic var type = DeckType.SYSTEM.rawValue
    
    override static func primaryKey() -> String? {
        return "word"
    }
}