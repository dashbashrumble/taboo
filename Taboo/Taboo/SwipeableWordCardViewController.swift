//
//  SwipeableWordCardViewController.swift
//  Taboo
//
//  Created by Zilingo on 21/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import ZLSwipeableViewSwift
import Cartography
import AVFoundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


protocol PauseScreenDelegate {
    func resume()
    func returnToHomeScreen()
}

class WordCaptures {
    var boogieWord: String = ""
    var boogieTime: Int = 0
    var gloryWord: String = ""
    var gloryTime: Int = 0
}

class SwipeableWordCardViewController: UIViewController, PauseScreenDelegate {

    // MARK: Outlets
    
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var teamScore: UILabel!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var correctButton: UIButton!
    @IBOutlet weak var tabooButton: UIButton!
    @IBOutlet weak var passButton: UIButton!
    
    // MARK: Private variables
    
    fileprivate var swipeableView: ZLSwipeableView!
    
    fileprivate var turnCorrect = 0
    fileprivate var turnTaboo = 0
    fileprivate var turnScore = 0
    fileprivate var turnPass = 0
    
    fileprivate var session = Session()
    fileprivate var wordCards = [WordCard]()
    fileprivate var currentTurn: Turn?
    fileprivate var tabooDec = Int(UserPreferences.instance.getTaboo()!)
    fileprivate var passAllowed = Int(UserPreferences.instance.getPassCount()!)!
    fileprivate var passRedeemed = 0
    
    fileprivate var count = Double(UserPreferences.instance.getTimeLimit()!)
    fileprivate var timer = Timer()
    
    // Stat capturing variables
    fileprivate var timeSinceLast = 0
    fileprivate var times = [Int]()
    fileprivate var words = [String]() // This array should maintain a list of all words that were displayed
    
    fileprivate var streak = 0
    fileprivate var maxStreak = 0
    
    // History recording variables
    fileprivate var displayedWords = [DisplayedWord]()
    fileprivate var currentWordProcessed = false
    
    var correctSound: AVAudioPlayer?
    var tabooSound: AVAudioPlayer?
    var passSound: AVAudioPlayer?
    var countdownSound: AVAudioPlayer?
    var timingOutSound: AVAudioPlayer?
    var timedOutSound: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.tb_betterBlue()
        timeSlider.isUserInteractionEnabled = false
        
        // Card configuration
        swipeableView = ZLSwipeableView()
        view.addSubview(swipeableView)
        
        swipeableView.didStart = {view, location in }
        
        swipeableView.swiping = {view, location, translation in
            let card = view as! CardView
            self.updateOverlay(card, distance: translation.x)
        }
        
        swipeableView.didEnd = {view, location in }
        
        swipeableView.didSwipe = {view, direction, vector in
            let card = view as! CardView
            self.updateOverlay(card, distance: vector.dx)
            
            if direction == Direction.Right {
                self.displayedWords.append(DisplayedWord(word: self.words[self.displayedWords.count], action: WordAction.correct))
                if !self.currentWordProcessed {
                    self.correctGuessAction()
                }
            } else if direction == Direction.Left {
                self.displayedWords.append(DisplayedWord(word: self.words[self.displayedWords.count], action: WordAction.taboo))
                if !self.currentWordProcessed {
                    self.tabooAction()
                }
            } else if direction == Direction.Up {
                self.displayedWords.append(DisplayedWord(word: self.words[self.displayedWords.count], action: WordAction.pass))
                if !self.currentWordProcessed {
                    self.passAction()
                    self.currentWordProcessed = false
                }
            }
            
            self.currentWordProcessed = false
        }
        swipeableView.didCancel = {view in
            let card = view as! CardView
            card.overlayView?.setClearColor()
        }
        swipeableView.didTap = {view, location in }
        
        swipeableView.didDisappear = { view in }
        
        constrain(swipeableView, view) { view1, view2 in
            view1.left == view2.left+50
            view1.right == view2.right-50
            view1.top == view2.top + 230
            view1.bottom == view2.bottom - 60
        }
        
        // Variable instantiations
        
        let currentDeck = UserPreferences.instance.getCurrentDeck()
        if currentDeck != nil {
            wordCards = DeckService.getWordsForDeck(currentDeck!)
        } else {
            wordCards = WordService.getAllWords()
        }
        
        timeSlider.minimumValue = Float(0)
        timeSlider.maximumValue = Float(UserPreferences.instance.getTimeLimit()!)!
        
//        self.navigationItem.setHidesBackButton(true, animated:true)
//        self.navigationItem.setLeftBarButton(nil, animated: true)
        
        if UserPreferences.instance.getSound() {
            
            if let sound = self.setupAudioPlayerWithFile("correct") {
                self.correctSound = sound
            }
            
            if let sound = self.setupAudioPlayerWithFile("taboo") {
                self.tabooSound = sound
            }
            
            if let sound = self.setupAudioPlayerWithFile("pass") {
                self.passSound = sound
            }
            
            if let sound = self.setupAudioPlayerWithFile("countdown") {
                self.countdownSound = sound
            }
            
            if let sound = self.setupAudioPlayerWithFile("time_finishing") {
                self.timingOutSound = sound
            }
            
            if let sound = self.setupAudioPlayerWithFile("timed_out") {
                self.timedOutSound = sound
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startNewTurn()
    }
    
    func updateOverlay(_ cardView: CardView, distance: CGFloat)
    {
        if (distance > 0) {
            cardView.overlayView?.setMode(OverlayMode.right)
        } else if (distance <= 0) {
            cardView.overlayView?.setMode(OverlayMode.left)
        }
        let overlayStrength = min(fabsf(Float(distance)) / 100, 0.4);
        cardView.overlayView!.alpha = CGFloat(overlayStrength)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        swipeableView.nextView = {
            return self.nextCardView()
        }
    }
    
    func updateSession(_ session: Session) {
        self.session = session
    }
    
    func startNewTurn() {
        // Set current turn
        currentTurn = session.getCurrentTurn()
        
        // Set team name
        teamName.text = session.getCurrentTeamName(currentTurn!)
        setTeamScore(0)
        
        // Set timing variables
        setTimeVariables(UserPreferences.instance.getTimeLimit()!)
        
        // Pass
        passRedeemed = Int(UserPreferences.instance.getPassCount()!)!
        setPassButtonText(passAllowed)
        
        // Show new card
        swipeableView.nextView = {
            return self.nextCardView()
        }
        self.view.layoutSubviews()
    }
    
    fileprivate func setTimeVariables(_ countdown: String) {
        count = Double(countdown)
        timeSlider.value = Float(countdown)!
        time.text = String(Int(countdown)!)
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(SwipeableWordCardViewController.update), userInfo: nil, repeats: true)
    }
    
    func nextCardView() -> UIView? {
        let wordCard = pickNewWord()
        
        let cardView = CardView(frame: swipeableView.bounds)
        
        let contentView = Bundle.main.loadNibNamed("CardView", owner: self, options: nil)!.first! as! CardView
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.backgroundColor = cardView.backgroundColor
        
        let clues = wordCard.clues
        var wordClues = [String]()
        for clue in clues {
            wordClues.append(clue.clue)
        }
        
        words.append(wordCard.word)
        contentView.setupWordCard(wordCard.word, clue1: wordClues[0], clue2: wordClues[1], clue3: wordClues[2], clue4: wordClues[3], clue5: wordClues[4])
        
        cardView.addSubview(contentView)
        cardView.addOverlay()
        
        constrain(contentView, cardView) { view1, view2 in
            view1.left == view2.left
            view1.top == view2.top
            view1.width == cardView.bounds.width
            view1.height == cardView.bounds.height
        }
        return cardView
    }
    
    fileprivate func pickNewWord() -> WordCard {
        let randomIndex = Int(arc4random_uniform(UInt32(wordCards.count)))
        return wordCards[randomIndex]
    }

    fileprivate func pause() {
        timer.invalidate()
    }
    
    internal func resume() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(SwipeableWordCardViewController.update), userInfo: nil, repeats: true)
    }
    
    func update() {
        if count > 6 {
            countdownSound?.play()
        } else if count <= 6 && count > 1 {
            timingOutSound?.play()
        } else if count == 1 {
            timedOutSound?.play()
        }
        
        if(count > 0) {
            count = count! - 1
            time.text = String(Int(count!))
            timeSlider.value = Float(count!)
            // Stat capturing
            timeSinceLast = timeSinceLast + 1
        } else {
            timer.invalidate()
            times.append(timeSinceLast)
            // Add to history
            session.addToHistory(words[displayedWords.count] + " (Timed Out)", timeTaken: timeSinceLast)
            displayedWords.append(DisplayedWord(word: words[displayedWords.count], action: WordAction.timeout))
            session.reportCorrect(turnCorrect)
            session.reportTaboo(turnTaboo)
            session.reportScore(turnScore)
            session.calculateAverageSecondsPerWord(times)
            session.reportWordCaptures(getWordCaptures())
            session.reportStreak(maxStreak)
            resetTurnVariables()
            
            if session.reportVictory() != nil {
                self.performSegue(withIdentifier: "Victory", sender: nil)
            } else {
                self.performSegue(withIdentifier: "ShowResults", sender: nil)
            }
        }
    }
    
    fileprivate func resetTurnVariables() {
        streak = 0
        maxStreak = 0
        
        timeSinceLast = 0
        times = [Int]()
        words = [String]()
        displayedWords = [DisplayedWord]()
        
        passRedeemed = 0
        
        turnCorrect = 0
        turnTaboo = 0
        turnScore = 0
        turnPass = 0
    }
    
    fileprivate func getWordCaptures() -> WordCaptures {
        let wordCaptures = WordCaptures()
        
        var max = -1000
        var min = 1000
        var maxWord = ""
        var minWord = ""
        
        var turnWords = [String]()
        for word in displayedWords {
            turnWords.append(word.word)
        }
        
        for i in 0..<times.count {
            if times[i] > max {
                max = times[i]
                maxWord = turnWords[i]
            }
            
            if times[i] < min {
                min = times[i]
                minWord = turnWords[i]
            }
        }
        
        wordCaptures.boogieWord = maxWord
        wordCaptures.boogieTime = max
        wordCaptures.gloryWord = minWord
        wordCaptures.gloryTime = min
        
        return wordCaptures
    }
    
    internal func returnToHomeScreen() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    fileprivate func setTeamScore(_ score: Int) {
        teamScore.text = "SCORE: " + String(score)
    }
    
    fileprivate func setPassButtonText(_ pass: Int) {
        passButton.setTitle("PASS: " + String(pass), for: UIControlState())
    }
    
    fileprivate func setPresentationStyleForSelfController(_ selfController: UIViewController, presentingController: UIViewController) {
        presentingController.providesPresentationContextTransitionStyle = true
        presentingController.definesPresentationContext = true
        presentingController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    }
    
    @IBAction func pauseGame(_ sender: AnyObject) {
        pause()
        self.performSegue(withIdentifier: "PauseScreen", sender: nil)
    }
    
    @IBAction func correctGuess(_ sender: AnyObject) {
        correctGuessAction()
        currentWordProcessed = true
        swipeableView.swipeTopView(inDirection: .Right)
    }
    
    fileprivate func correctGuessAction() {
        correctSound?.play()
        
        turnCorrect = turnCorrect + 1
        turnScore = turnScore + 1
        streak = streak + 1
        updateMaxStreak(streak)
        setTeamScore(turnScore)
        
        // Add to history
        session.addToHistory(words[displayedWords.count] + " (Correct)", timeTaken: timeSinceLast)
        
        // Stat capturing
        captureTimeSinceLast()
    }
    
    @IBAction func taboo(_ sender: AnyObject) {
        tabooAction()
        currentWordProcessed = true
        swipeableView.swipeTopView(inDirection: .Left)
    }
    
    fileprivate func tabooAction() {
        tabooSound?.play()
        
        turnTaboo = turnTaboo + 1
        turnScore = turnScore - tabooDec!
        streak = 0
        setTeamScore(turnScore)
        
        // Add to history
        session.addToHistory(words[displayedWords.count] + " (Taboo)", timeTaken: timeSinceLast)
        
        captureTimeSinceLast()
    }
    
    @IBAction func pass(_ sender: AnyObject) {
        if passRedeemed > 0 {
            passAction()
            currentWordProcessed = true
            swipeableView.swipeTopView(inDirection: .Up)
        }
    }
    
    fileprivate func passAction() {
        passSound?.play()
        
        passRedeemed = passRedeemed - 1
        turnPass = turnPass + 1
        setPassButtonText(passRedeemed)
        
        // Add to history
        session.addToHistory(words[displayedWords.count] + " (Pass)", timeTaken: timeSinceLast)
        
        captureTimeSinceLast()
    }
    
    fileprivate func captureTimeSinceLast() {
        times.append(timeSinceLast)
        timeSinceLast = 0
    }
    
    fileprivate func updateMaxStreak(_ streak: Int) {
        if streak > maxStreak {
            maxStreak = streak
        }
    }
    
    func setupAudioPlayerWithFile(_ file: NSString) -> AVAudioPlayer? {
        
        let url = Bundle.main.url(forResource: file as String, withExtension: "mp3")!
        var audioPlayer: AVAudioPlayer?
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
    
    // MARK: - Navigation


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "PauseScreen":
            let destinationVC = segue.destination as! PauseScreenViewController
            destinationVC.setDelegate(self)
            setPresentationStyleForSelfController(self, presentingController: destinationVC)
            break
        case "ShowResults":
            let destinationVC = segue.destination as! ResultViewController
            destinationVC.setSession(session)
            break
        case "Victory":
            let destinationVC = segue.destination as! VictoryViewController
            destinationVC.updateSession(session)
            break
        default: break
        }
    }
    
}

class DisplayedWord {
    var word = ""
    var action = WordAction.timeout
    
    init(word: String, action: WordAction) {
        self.word = word
        self.action = action
    }
}

enum WordAction {
    case correct
    case taboo
    case pass
    case timeout
}

