//
//  ColorService.swift
//  Taboo
//
//  Created by Zilingo on 09/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import UIKit

class ColorService {
    let hexValue : String
    var red : Int?
    var green : Int?
    var blue : Int?
    
    init(hexValue: String){
        self.hexValue = hexValue
        
    }
    
    fileprivate func  getHexString(_ first: Int, last: Int)-> String {
        return hexValue.substring(with: (hexValue.characters.index(hexValue.startIndex, offsetBy: first) ..< hexValue.characters.index(hexValue.startIndex, offsetBy: last)))
    }
    
    func getRed()-> CGFloat {
        red = Int(strtoul(getHexString(1,last: 3), nil, 16))
        return CGFloat(Double(red!)/Double(255))
    }
    
    func getGreen()-> CGFloat{
        green =  Int(strtoul(getHexString(3,last: 5), nil, 16))
        return CGFloat(Double(green!)/Double(255))
    }
    
    func getBlue()-> CGFloat{
        blue =  Int(strtoul(getHexString(5,last: 7), nil, 16))
        return CGFloat(Double(blue!)/Double(255))
    }
    
    func getUIColor() -> UIColor {
        return UIColor(red: getRed(), green: getGreen(), blue: getBlue(), alpha: 1)
    }
    
}
