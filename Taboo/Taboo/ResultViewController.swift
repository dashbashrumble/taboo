//
//  ResultViewController.swift
//  Taboo
//
//  Created by Zilingo on 18/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
//import JLToast
import Toaster

class ResultViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var correctValue: UILabel!
    @IBOutlet weak var tabooValue: UILabel!
    @IBOutlet weak var scoreValue: UILabel!
    @IBOutlet weak var aspwValue: UILabel!
    @IBOutlet weak var bwValue: UILabel!
    @IBOutlet weak var gwValue: UILabel!
    @IBOutlet weak var lpsValue: UILabel!

    @IBOutlet weak var teamA: UILabel!
    @IBOutlet weak var teamB: UILabel!
    @IBOutlet weak var teamAScore: UILabel!
    @IBOutlet weak var teamBScore: UILabel!
    
    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var player: UITextField!
    @IBOutlet weak var mvpHelperLabel: UILabel!
    
    fileprivate var session: Session?
    
    fileprivate var players = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        self.navigationItem.setHidesBackButton(true, animated:true)
        
        correctValue.text = String(session!.turnCorrect)
        tabooValue.text = String(session!.turnTaboo) + "(x-" + UserPreferences.instance.getTaboo()! + ")"
        scoreValue.text = String(session!.turnScore)
        aspwValue.text = String(session!.aspw)
        bwValue.text = session!.getBoogieString()
        gwValue.text = session!.getGloryString()
        lpsValue.text = String(session!.streak)
        
        teamA.text = session!.teamA
        teamB.text = session!.teamB
        teamAScore.text = String(session!.teamATotalScore)
        teamBScore.text = String(session!.teamBTotalScore)
        
        players = session!.getCurrentTeamPlayers()
        if players.count == 0 {
            pickerView.isHidden = true
        } else {
            player.text = players[0]
        }
        
        pickerView.dataSource = self
        pickerView.delegate = self
        player.delegate = self
        
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(TeamNamesViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        if session!.mvpSkipped || (session!.isLastRound) {
            pickerView.isHidden = true
            player.isHidden = true
            mvpHelperLabel.isHidden = true
        }
        
        if session!.isLastRound {
            continueButton.isEnabled = false
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ResultViewController.removeKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.barTintColor = UIColor.tb_betterBlue()
        self.navigationController?.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func removeKeyboard() {
        view.endEditing(true)
    }
    
    func setSession(_ session: Session) {
        self.session = session
    }
    
    func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @IBAction func returnToHomeScreen(_ sender: AnyObject) {
        showQuitGameAlert()
    }
    
    @IBAction func continueGame(_ sender: AnyObject) {
        addNewPlayer()
        session!.updatePlayerScores(player.text, score: session?.turnScore)
        session!.resetTurnVariables()
        
        for vc in (self.navigationController?.viewControllers)! {
            if vc is Countdown3ViewController {
                let countdownVC = vc as! Countdown3ViewController
                countdownVC.updateSession(session!)
                countdownVC.reset()
                countdownVC.scheduleTimers()
                self.navigationController?.popToViewController(countdownVC, animated: true)
            }
        }
    }
    
    @IBAction func showWordHistory(_ sender: AnyObject) {
        self.performSegue(withIdentifier: "ShowHistory", sender: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return players.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return players[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        
        label.font = UIFont(name: "Futura-Medium", size: 13)!
        label.text = players[row]
        label.textColor = UIColor.tb_yellow()
        label.textAlignment = .center
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        player.text = players[row]
    }
    
    func addNewPlayer() {
        if player.text != nil && !player.text!.isEmpty {
            if !players.contains(player.text!) {
                session!.addNewPlayer(player.text!)
            }
        } else {
            session!.mvpSkipped = true
        }
    }
    
    func showQuitGameAlert() {
        let alertController = UIAlertController(title: "Quit game?", message: "Are you sure you want to quit the game? You will lose all your progress!", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.navigationController?.popToRootViewController(animated: true)
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { (_) in }
        
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! HistoryViewController
        destinationVC.updateSession(session!)
    }

}
