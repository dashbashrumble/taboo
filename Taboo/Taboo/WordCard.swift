//
//  WordCard.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import RealmSwift

class WordCard: Object {
    dynamic var id = UUID().uuidString
    dynamic var word = ""
    var clues = List<Clue>()
    dynamic var difficulty = ""
    dynamic var category = ""
    dynamic var deck: Deck?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
