//
//  WordCardJson.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import ObjectMapper

class WordCardJson: Mappable {
    fileprivate(set) var word: String?
    fileprivate(set) var clues = [String]()
    fileprivate var difficulty: String?
    fileprivate(set) var wordDifficulty: WordDifficulty?
    fileprivate var category: String?
    fileprivate(set) var wordCategory: WordCategory?
    
    required init(map: Map){}
    
    func mapping(map: Map) {
        word <- map["word"]
        clues <- map["clues"]
        
        difficulty <- map["difficulty"]
        wordDifficulty = WordDifficulty(rawValue: difficulty!)
        
        category <- map["category"]
        wordCategory = WordCategory(rawValue: category!)
    }
}
