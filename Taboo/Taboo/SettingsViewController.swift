//
//  SettingsViewController.swift
//  Taboo
//
//  Created by Zilingo on 09/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SettingsViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var winningPointsLabel: UILabel!
    @IBOutlet weak var tabooLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
    @IBOutlet weak var soundLabel: UILabel!
    
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var winningPointsSlider: UISlider!
    @IBOutlet weak var tabooSlider: UISlider!
    @IBOutlet weak var passSlider: UISlider!
    @IBOutlet weak var soundSwitch: UISwitch!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    fileprivate let timeStep = Float(30)
    fileprivate let tabooStep = Float(1)
    fileprivate let winnningPointsStep = Float(25)
    fileprivate let passStep = Float(1)
    
    var interstitial: GADInterstitial!
    
    func createAndLoadInterstitial() -> GADInterstitial {
        let interstitial = GADInterstitial(adUnitID: "ca-app-pub-9043117893678177/1469976146")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-9043117893678177/9184126279"
        bannerView.rootViewController = self
        bannerView.load(GADRequest())
        bannerView.delegate = self
        bannerView.alpha = 0.0
        
        interstitial = createAndLoadInterstitial()
        
        customizeNavigationBar()
        setLabelTextsAndSliderValues()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.interstitial.isReady {
            self.interstitial.present(fromRootViewController: self)
        }
    }
    
    fileprivate func customizeNavigationBar() {
        self.title = "Settings"
        self.navigationController?.navigationBar.barTintColor = UIColor.tb_betterBlue()
        self.navigationController!.navigationBar.tintColor = UIColor.tb_yellow()
    }
    
    fileprivate func setLabelTextsAndSliderValues() {
        if let time = UserPreferences.instance.getTimeLimit() {
            setTimeLabel(time)
            timeSlider.value = Float(time)!
        } else {
            setTimeLabel("60")
            timeSlider.value = Float("60")!
        }
        
        if let winningPoints = UserPreferences.instance.getWinningPoints() {
            setWinningPointsLabel(winningPoints)
            winningPointsSlider.value = Float(winningPoints)!
        } else {
            setWinningPointsLabel("50")
            winningPointsSlider.value = Float("50")!
        }
        
        if let taboo = UserPreferences.instance.getTaboo() {
            setTabooLabel(taboo)
            tabooSlider.value = Float(taboo)!
        } else {
            setTabooLabel("3")
            tabooSlider.value = Float("3")!
        }
        
        if let pass = UserPreferences.instance.getPassCount() {
            setPassLabel(pass)
            passSlider.value = Float(pass)!
        } else {
            setPassLabel("3")
            passSlider.value = Float("3")!
        }
        
        if UserPreferences.instance.getSound() {
            soundSwitch.setOn(true, animated: true)
            soundLabel.text = "Sound: On"
        } else {
            soundSwitch.setOn(false, animated: true)
            soundLabel.text = "Sound: Off"
        }
    }
    
    @ objc fileprivate func animateWheel() {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0.0
        rotationAnimation.toValue = M_PI
        rotationAnimation.duration = 1
    }
    
    @IBAction func timeValueChanged(_ sender: AnyObject) {
        let slider = sender as! UISlider
        let roundedValue = round(slider.value / timeStep) * timeStep
        slider.value = roundedValue
        
        setTimeLabel(String(Int(roundedValue)))
        UserPreferences.instance.setTimeLimit(String(Int(roundedValue)))
    }
    
    @IBAction func winningPointsValueChanged(_ sender: AnyObject) {
        let slider = sender as! UISlider
        let roundedValue = round(slider.value / winnningPointsStep) * winnningPointsStep
        slider.value = roundedValue
        
        setWinningPointsLabel(String(Int(roundedValue)))
        UserPreferences.instance.setWinningPoints(String(Int(roundedValue)))
    }

    @IBAction func tabooValueChanged(_ sender: AnyObject) {
        let slider = sender as! UISlider
        let roundedValue = round(slider.value / tabooStep) * tabooStep
        slider.value = roundedValue
        
        setTabooLabel(String(Int(roundedValue)))
        UserPreferences.instance.setTaboo(String(Int(roundedValue)))
    }

    @IBAction func passValueChanged(_ sender: AnyObject) {
        let slider = sender as! UISlider
        let roundedValue = round(slider.value / passStep) * passStep
        slider.value = roundedValue
        
        setPassLabel(String(Int(roundedValue)))
        UserPreferences.instance.setPassCount(String(Int(roundedValue)))
    }
    
    @IBAction func soundSwitchValueChanged(_ sender: AnyObject) {
        let soundStatus = sender as! UISwitch
        if soundStatus.isOn {
            soundLabel.text = "Sound: On"
            UserPreferences.instance.setSound(true)
        } else {
            soundLabel.text = "Sound: Off"
            UserPreferences.instance.setSound(false)
        }
    }
    
    fileprivate func setTimeLabel(_ time: String) {
        self.timeLabel.text = "Time: " + time
    }
    
    fileprivate func setWinningPointsLabel(_ winningPoints: String) {
        self.winningPointsLabel.text = "Winning Points: " + winningPoints
    }
    
    fileprivate func setTabooLabel(_ taboo: String) {
        self.tabooLabel.text = "Veto: " + taboo
    }
    
    fileprivate func setPassLabel(_ pass: String) {
        self.passLabel.text = "Pass: " + pass
    }
}

extension SettingsViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        bannerView.alpha = 0
        UIView.animate(withDuration: 1, animations: {
            bannerView.alpha = 1
        })
    }
}

extension SettingsViewController: GADInterstitialDelegate {
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
    }
}
