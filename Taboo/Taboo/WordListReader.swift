//
//  WordListReader.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class WordListReader {
    static func saveAllWords() {
        if let path = Bundle.main.path(forResource: "wordlist", ofType: "json") {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
                let datastring = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                let wordList = Mapper<WordListJson>().map(JSONString: datastring! as String)
                
                let version = wordList?.version
                let savedVersion = UserPreferences.instance.getWordListVersion()
                
                if let currentVersion = savedVersion {
                    if HelperFunctions.isGreaterVersion(version!, oldVersion: currentVersion) {
                        saveWordsToDB(wordList?.wordCards)
                        UserPreferences.instance.setWordListVersion(currentVersion)
                    }
                } else {
                    saveWordsToDB(wordList?.wordCards)
                }
            }
        }
    }
    
    static func saveWordsToDB(_ wordList: [WordCardJson]?) {
        let realm = RealmManager.instance?.getRealm()
        
        realm?.beginWrite()
        if let list = wordList {
            for card in list {
                let wordCard = WordCard()
                wordCard.word = card.word!
                
                let clues = List<Clue>()
                for clueJson in card.clues {
                    let clue = Clue()
                    clue.clue = clueJson
                    clues.append(clue)
                }
                wordCard.clues = clues
                
                wordCard.difficulty = (card.wordDifficulty?.rawValue)!
                wordCard.category = (card.wordCategory?.rawValue)!
                
                let deck = DeckService.getOrCreateDeck((card.wordCategory?.description)!, deckType: .SYSTEM, ongoingRealmTransaction: true)
                wordCard.deck = deck
                
                realm?.add(wordCard, update: false)
                
                deck.cards.append(wordCard)
                realm?.add(deck, update: true)
            }
        }
        
        try! realm?.commitWrite()
    }
}

