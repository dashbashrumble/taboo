//
//  HelperFunctions.swift
//  Taboo
//
//  Created by Zilingo on 08/10/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import Foundation

class HelperFunctions {
    
    static func isGreaterVersion(_ newVersion: String, oldVersion: String) -> Bool {
        let newArray =  newVersion.components(separatedBy: ".").map(castAsInt)
        let oldArray = oldVersion.components(separatedBy: ".").map(castAsInt)
        
        for index in  0...2 {
            if (newArray[index] > oldArray[index]){
                return true
            } else if newArray[index] < oldArray[index] {
                return false
            }
        }
        
        return false
    }
    
    static func castAsInt(_ stringInt : String) -> Int {
        return Int(stringInt)!
    }
}
