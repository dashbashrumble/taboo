//
//  DeckHeaderCell.swift
//  Taboo
//
//  Created by Zilingo on 01/12/16.
//  Copyright © 2016 Vendetta Works. All rights reserved.
//

import UIKit

class DeckHeaderCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.isUserInteractionEnabled = false
    }
    
    func updateCell(_ headerText: String) {
        headerLabel.text = headerText
    }
}
